import org.apache.tools.ant.types.resources.selectors.Date

plugins {
    `java-library`
    `maven-publish`
    id("com.github.johnrengelman.shadow") version "2.0.2"
    id("com.jfrog.bintray") version "1.8.4"
}

group = "be.epireyn"
version = "2.3"

repositories {
    mavenCentral()
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven("https://oss.sonatype.org/content/repositories/snapshots")
    maven("https://dl.bintray.com/epireyn/maven")
    mavenLocal()
}

dependencies {
    testCompile("junit", "junit", "4.12")
    compileOnly("org.spigotmc:spigot-api:1.13.2-R0.1-SNAPSHOT")
    implementation("be.epireyn:SpigotHelper:0.3.2")
    implementation("com.google.code.gson:gson:2.8.5")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["java"])
        }
    }
}

bintray {
    user = "epireyn"
    key = System.getenv("BINTRAY_API_KEY")

    with (pkg) {
        repo = "maven"
        name = project.name
        setLicenses("Apache-2.0")
        websiteUrl = "https://gitlab.com/epireyn/mctranslator/"
        issueTrackerUrl = "https://gitlab.com/epireyn/mctranslator/issues"
        vcsUrl = "https://gitlab.com/epireyn/mctranslator/"
        with (version) {
            name = project.version.toString()
            released = Date().datetime
        }
    }
    setPublications("default")
    publish = true
}