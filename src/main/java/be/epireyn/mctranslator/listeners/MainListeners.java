package be.epireyn.mctranslator.listeners;

import be.epireyn.mctranslator.MCTranslator;
import be.epireyn.mctranslator.api.Languages;
import be.epireyn.mctranslator.api.PlayerLanguage;
import be.epireyn.mctranslator.api.TranslationHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class MainListeners implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent event) {
        Languages from = PlayerLanguage.getPlayerLanguage(event.getPlayer());
        if ((boolean) MCTranslator.INSTANCE.getConfiguration().get("show-language-in-chat")) {
            event.setFormat(ChatColor.WHITE + "" + ChatColor.BOLD + "[" + ChatColor.RESET + "" + ChatColor.BLUE + from.getName() + ChatColor.WHITE + "" + ChatColor.BOLD + "] " + ChatColor.RESET + event.getFormat());
        }
        event.setCancelled(true);
        for (UUID receiverId : new ArrayList<>(MCTranslator.INSTANCE.getTranslatedPlayers())) {
            Player receiver = Bukkit.getPlayer(receiverId);
            if (receiver == null) {
                MCTranslator.INSTANCE.getTranslatedPlayers().remove(receiverId);
                continue;
            }
            if (from != PlayerLanguage.getPlayerLanguage(receiver, from) && event.getPlayer().getUniqueId() != receiverId) {
                try {
                    receiver.sendMessage(event.getFormat().replace("%1$s", event.getPlayer().getDisplayName())
                            .replace("%2$s", TranslationHandler.translate(event.getMessage(), from, PlayerLanguage.getPlayerLanguage(receiver), MCTranslator.API_KEY)));
                } catch (IOException e) {
                    event.getPlayer().sendMessage(ChatColor.RED + "Error when translating your message: " + e.getMessage());
                    receiver.sendMessage(ChatColor.BLUE + "Translate couldn't be made: " + ChatColor.RESET + event.getFormat().replace("%1$s", event.getPlayer().getDisplayName())
                            .replace("%2$s", event.getMessage()));
                }
            } else {
                receiver.sendMessage(event.getFormat().replace("%1$s", event.getPlayer().getDisplayName())
                        .replace("%2$s", event.getMessage()));
            }

        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        MCTranslator.INSTANCE.getTranslatedPlayers().remove(event.getPlayer().getUniqueId());
        if (MCTranslator.INSTANCE.getPlayerSave().get(event.getPlayer().getUniqueId().toString()) != null)
            if ((boolean) MCTranslator.INSTANCE.getPlayerSave().get(event.getPlayer().getUniqueId().toString()))
            MCTranslator.INSTANCE.getTranslatedPlayers().add(event.getPlayer().getUniqueId());

            if (MCTranslator.INSTANCE.getTranslatedPlayers().contains(event.getPlayer().getUniqueId()))
                event.getPlayer().sendMessage(ChatColor.GREEN + "Translator enabled!");
            else
                event.getPlayer().sendMessage(ChatColor.DARK_RED + "Translator disabled!");
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        MCTranslator.INSTANCE.getPlayerSave().set(event.getPlayer().getUniqueId().toString(), MCTranslator.INSTANCE.getTranslatedPlayers().contains(event.getPlayer().getUniqueId()));
        MCTranslator.INSTANCE.getTranslatedPlayers().remove(event.getPlayer().getUniqueId());
    }
}
