package be.epireyn.mctranslator.commands;

import be.epireyn.mctranslator.MCTranslator;
import be.epireyn.mctranslator.api.Languages;
import be.epireyn.mctranslator.api.PlayerLanguage;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainCommands implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (cmd.getName().equals("translator")) {
                if (args.length > 1) {
                    player.sendMessage(ChatColor.RED + "Error in command:");
                    player.sendMessage(ChatColor.RED + "/tr [language|?]");
                    return true;
                }

                if (args.length == 1) {
                    if (args[0].equals("?")) {
                        Languages languages = PlayerLanguage.getPlayerLanguage(player);
                        player.sendMessage(ChatColor.BLUE + languages.getName() + "/" + languages.getCode());
                        return true;
                    }
                    Languages language;
                    if (Languages.getEnum(args[0]) == Languages.UNKNOWN && Languages.getLanguage(args[0]) == Languages.UNKNOWN)
                        player.sendMessage(ChatColor.RED + "Unknown language! Are you sure it is supported?");
                    else {
                        language = Languages.getLanguage(args[0].toLowerCase());
                        if (language == Languages.UNKNOWN)
                            language = Languages.getEnum(args[0].toLowerCase());
                        if (MCTranslator.INSTANCE.getAcceptedLanguages().contains(language)) {
                            PlayerLanguage.setPlayerLanguage(player, language);
                            player.sendMessage(ChatColor.GREEN + "Your language had setted to " + language.getName());
                        } else {
                            player.sendMessage(ChatColor.DARK_RED + "Your language isn't accepted by the server!");
                        }
                    }
                } else {
                    if (MCTranslator.INSTANCE.getTranslatedPlayers().remove(player.getUniqueId()))
                        player.sendMessage(ChatColor.DARK_RED + "Translator disabled!");
                    else {
                        MCTranslator.INSTANCE.getTranslatedPlayers().add(player.getUniqueId());
                        player.sendMessage(ChatColor.GREEN + "Translator enabled!");
                    }
                }
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Only players can perform this command!");
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> tab = new ArrayList<>();
        for (Languages l : MCTranslator.INSTANCE.getAcceptedLanguages())
            tab.add(l.getName());
        return tab;
    }
}
