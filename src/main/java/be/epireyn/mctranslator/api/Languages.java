package be.epireyn.mctranslator.api;

import java.util.HashSet;
import java.util.Set;

public enum Languages {

    AZERBAIJAN("az"),
    ALBANIAN("sq"),
    AMHARIC("am"),
    ARABIC("ar"),
    ARMENIAN("hy"),
    AFRIKAANS("af"),
    BASQUE("eu"),
    BASHKIR("ba"),
    BELARUSIAN("be"),
    BENGALI("bn"),
    BURMESE("my"),
    BULGARIAN("bg"),
    BOSNIAN("bs"),
    CHINESE("zh"),
    CROATIAN("hr"),
    CZECH("cs"),
    DUTCH("nl"),
    DANISH("da"),
    ENGLISH("en"),
    ESTONIAN("et"),
    ESPERANTO("eo"),
    FINNISH("fi"),
    FRENCH("fr"),
    GALICIAN("gl"),
    HILLMARI("mrj"),
    WELSH("cy"),
    HUNGARIAN("hu"),
    VIETNAMESE("vi"),
    HAITIAN("ht"),
    GREEK("el"),
    GEORGIAN("ka"),
    GUJARATI("gu"),
    HEBREW("he"),
    YIDDISH("yi"),
    INDONESIAN("id"),
    IRISH("ga"),
    ITALIAN("it"),
    ICELANDIC("is"),
    SPANISH("es"),
    KAZAKH("kk"),
    KANNADA("kn"),
    CATALAN("ca"),
    KYRGYZ("ky"),
    KOREAN("ko"),
    XHOSA("xh"),
    KHMER("km"),
    LAOTIAN("lo"),
    LATIN("la"),
    LATVIAN("lv"),
    LITHUANIAN("lt"),
    LUXEMBOURGISH("lb"),
    MALAGASY("mg"),
    MALAY("ms"),
    MALAYALAM("ml"),
    MALTESE("mt"),
    MACEDONIAN("mk"),
    MAORI("mi"),
    MARATHI("mr"),
    MARI("mhr"),
    MONGOLIAN("mn"),
    GERMAN("de"),
    NEPALI("ne"),
    NORWEGIAN("no"),
    PUNJABI("pa"),
    PAPIAMENTO("pap"),
    PERSIAN("fa"),
    POLISH("pl"),
    PORTUGUESE("pt"),
    ROMANIAN("ro"),
    RUSSIAN("ru"),
    CEBUANO("ceb"),
    SERBIAN("sr"),
    SINHALA("si"),
    SLOVAKIAN("sk"),
    SLOVENIAN("sl"),
    SWAHILI("sw"),
    SUNDANESE("su"),
    TAJIK("tg"),
    THAI("th"),
    TAGALOG("tl"),
    TAMIL("ta"),
    TATAR("tt"),
    TELUGU("te"),
    TURKISH("tr"),
    UDMURT("udm"),
    UZBEK("uz"),
    UKRAINIAN("uk"),
    URDU("ur"),
    HINDI("hi"),
    SWEDISH("sv"),
    SCOTTISH("gd"),
    JAVANESE("jv"),
    JAPANESE("ja"),
    UNKNOWN("un");

    private final String code;

    Languages(String code) {
        this.code = code;
    }

    public String getName() {
        String name = this.name().substring(0, 1).toUpperCase();
        name += this.name().substring(1).toLowerCase();
        return name;
    }

    public String getCode() {
        return code;
    }

    public static Languages getLanguage(String code) {
        for (Languages language : values())
            if (code.equals(language.getCode()))
                return language;

        return UNKNOWN;
    }

    public static Languages[] getLanguages(String... codes) {
        Set<Languages> languages = new HashSet<>();
        for (String code : codes) {
            languages.add(getLanguage(code));
        }
        return languages.toArray(new Languages[]{});
    }

    public static Languages getEnum(String name) {
        for (Languages language : values())
            if (name.toUpperCase().equals(language.getName().toUpperCase()))
                return language;

        return UNKNOWN;
    }

    @Override
    public String toString() {
        return this.name() + " (" + this.getName() + ")";
    }
}
