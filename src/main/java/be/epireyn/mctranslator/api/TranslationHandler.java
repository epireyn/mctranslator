package be.epireyn.mctranslator.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

public class TranslationHandler {

    private TranslationHandler() {}

    public static String translate(String msg, Languages from, Languages to, String apiKey) throws IOException {
        String link = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%key%&text=%text%&lang=%language%";
        String language;
        if (from == Languages.UNKNOWN) language = to.getCode();
        else language = from.getCode() + "-" + to.getCode();
        msg = URLEncoder.encode(msg, "UTF-8");
        URL url = new URL(link.replace("%language%", language).replace("%key%", apiKey).replace("%text%", msg));
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
        httpsURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        BufferedReader reader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream(), "UTF-8"));
        String input = reader.readLine();
        reader.close();
        JsonObject object = new JsonParser().parse(input).getAsJsonObject();
        JsonArray array = object.getAsJsonArray("text");
        StringBuilder stringBuilder = new StringBuilder();
        for (JsonElement member : array) {
            stringBuilder.append(member.getAsString());
        }
        return stringBuilder.toString().trim();
    }

    public static Languages detect(String msg, String apiKey) throws IOException {
        String link = "https://translate.yandex.net/api/v1.5/tr.json/detect?key=%key%&text=%text%";
        msg = URLEncoder.encode(msg, "UTF-8");
        URL url = new URL(link.replace("%key%", apiKey).replace("%text%", msg));
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
        httpsURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        if (httpsURLConnection.getResponseCode() == 200) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
            String input = reader.readLine();
            reader.close();

            JsonObject object = new JsonParser().parse(input).getAsJsonObject();
            return Languages.getLanguage(object.get("lang").getAsString());
        } else if (httpsURLConnection.getResponseCode() == 401) {
            throw new TranslationException(apiKey, TranslationException.TranslationExceptionType.WRONG_API_KEY);
        } else if (httpsURLConnection.getResponseCode() == 402) {
            throw new TranslationException(apiKey, TranslationException.TranslationExceptionType.BLOCKED_API_KEY);
        } else if (httpsURLConnection.getResponseCode() == 404) {
            throw new TranslationException(apiKey, TranslationException.TranslationExceptionType.EXCEEDED_DAILY_LIMIT);
        } else if (httpsURLConnection.getResponseCode() == 413) {
            throw new TranslationException(apiKey, TranslationException.TranslationExceptionType.EXCEED_MAXIMUM_TEXT_SIZE);
        } else if (httpsURLConnection.getResponseCode() == 422) {
            throw new TranslationException(apiKey, TranslationException.TranslationExceptionType.TEXT_CANNOT_TRANSLATED);
        } else if (httpsURLConnection.getResponseCode() == 501) {
            throw new TranslationException(apiKey, TranslationException.TranslationExceptionType.TRANSLATION_NOT_SUPPORTED);
        }
        return null;
    }

}
