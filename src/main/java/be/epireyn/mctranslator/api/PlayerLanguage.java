package be.epireyn.mctranslator.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerLanguage {
    private static Map<UUID, Languages> datas = new HashMap<>();

    public static Languages getPlayerLanguage(Player player) {
        return getPlayerLanguage(player, Languages.UNKNOWN);
    }

    public static void setPlayerLanguage(Player player, Languages language) {
        if (!datas.containsKey(player.getUniqueId()))
            datas.put(player.getUniqueId(), language);
        else
            datas.replace(player.getUniqueId(), language);
    }

    public static void fromJson(Map<UUID, Languages> datas) {
        PlayerLanguage.datas = datas;
    }

    public static Languages getPlayerLanguage(Player player, Languages def) {
        if (datas.containsKey(player.getUniqueId())) {
            return datas.get(player.getUniqueId());
        }
        datas.put(player.getUniqueId(), def);
        return def;
    }

    public static String toJson() {
        Gson gson = new Gson();
        return gson.toJson(datas, new TypeToken<HashMap<UUID, Languages>>(){}.getType());
    }
}

