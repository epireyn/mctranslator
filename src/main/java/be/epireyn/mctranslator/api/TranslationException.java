package be.epireyn.mctranslator.api;

import java.io.IOException;

public class TranslationException extends IOException {

    public TranslationException(String apiKey, TranslationExceptionType type) {
        super(type.getException().replace("%apikey%", apiKey));
    }

    public enum TranslationExceptionType {
        WRONG_API_KEY("The Api Key (%apikey%) is wrong!"),
        BLOCKED_API_KEY("The Api Key (%apikey%) is blocked!"),
        EXCEEDED_DAILY_LIMIT("Exceeded the daily limit on the amount of translated text!"),
        EXCEED_MAXIMUM_TEXT_SIZE("Exceeded the maximum text size!"),
        TEXT_CANNOT_TRANSLATED("The text cannot be translated!"),
        TRANSLATION_NOT_SUPPORTED("The specified translation is not supported!");

        private final String exception;

        TranslationExceptionType(String exception) {
            this.exception = exception;
        }

        public String getException() {
            return exception;
        }
    }
}


