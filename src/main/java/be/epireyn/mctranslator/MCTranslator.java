package be.epireyn.mctranslator;

import be.epireyn.mctranslator.api.Languages;
import be.epireyn.mctranslator.api.PlayerLanguage;
import be.epireyn.mctranslator.commands.MainCommands;
import be.epireyn.mctranslator.listeners.MainListeners;
import be.epireyn.spigothelper.configuration.SpigotConfiguration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginLogger;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.*;

public class MCTranslator extends JavaPlugin {

    public static MCTranslator INSTANCE;
    private final List<Languages> acceptedLanguages = new ArrayList<>();
    private SpigotConfiguration configuration, playerSave;
    public static String API_KEY;
    private final List<UUID> translatedPlayers = new ArrayList<>();
    private final File save = new File(getDataFolder(), "datas.json");

    @Override
    public void onLoad() {
        super.onLoad();
        INSTANCE = this;
        if (save.exists()) {
            if (save.length() != 0) {
                Gson gson = new Gson();
                try {
                    PlayerLanguage.fromJson(gson.fromJson(new FileReader(save), new TypeToken<HashMap<UUID, Languages>>(){}.getType()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onEnable() {
        super.onEnable();
        Listener main = new MainListeners();
        CommandExecutor mainCommands = new MainCommands();
        Bukkit.getPluginManager().registerEvents(main, this);
        getCommand("translator").setExecutor(mainCommands);

        PluginLogger.getLogger("MCTranslator").info("MCTranslator is powered by Yandex.Translate (http://translate.yandex.com/)");
        try {
            configuration = new SpigotConfiguration(new File(getDataFolder(), "config.yml"), conf -> {
                conf.set("accepted-languages", "all");
                conf.set("show-language-in-chat", true);
                conf.set("api-key", "YOUR API KEY");
            });
            if (configuration.get("api-key") == null || ((String) configuration.get("api-key")).equalsIgnoreCase("YOUR API KEY")) {
                PluginLogger.getLogger("MCTranslator").info(ChatColor.RED + "Api key is required! Disabling plugin...");
                getPluginLoader().disablePlugin(this);
            } else {
                API_KEY = (String) configuration.get("api-key");
            }
            if (configuration.get("accepted-languages") != null) {

                if (configuration.get("accepted-languages").equals("all")) {
                    acceptedLanguages.addAll(Arrays.asList(Languages.values()));
                } else {

                    if (configuration.get("accepted-languages") instanceof List) {
                        List<String> codes = (List<String>) configuration.get("accepted-languages");
                        acceptedLanguages.addAll(Arrays.asList(Languages.getLanguages(codes.toArray(new String[]{}))));
                    } else {
                        PluginLogger.getLogger("MCTranslator").info(ChatColor.RED + "Error in accepted-languages format!");
                        acceptedLanguages.addAll(Arrays.asList(Languages.values()));
                    }
                }
            }
        } catch (IOException e) {
            PluginLogger.getLogger("MCTranslator").info(ChatColor.RED + "Error on loading config file: " + e.getMessage());
            PluginLogger.getLogger("MCTranslator").info(ChatColor.RED + "Disabling plugin...");
            getPluginLoader().disablePlugin(this);
        }
        try {
            playerSave = new SpigotConfiguration(new File(getDataFolder(), "players.yml"), null);

        } catch (IOException e) {
            PluginLogger.getLogger("MCTranslator").info(ChatColor.RED + "Error on loading players' saves file: " + e.getMessage());
            PluginLogger.getLogger("MCTranslator").info(ChatColor.RED + "Disabling plugin...");
            getPluginLoader().disablePlugin(this);
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (MCTranslator.INSTANCE.getPlayerSave().get(player.getUniqueId().toString()) != null)
                if ((boolean) MCTranslator.INSTANCE.getPlayerSave().get(player.getUniqueId().toString()))
                    MCTranslator.INSTANCE.getTranslatedPlayers().add(player.getUniqueId());

        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
        if (!save.getParentFile().exists()) save.getParentFile().mkdirs();
        try {
            save.delete();
            save.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(save));
            writer.write(PlayerLanguage.toJson());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            getPlayerSave().set(p.getUniqueId().toString(), getTranslatedPlayers().contains(p.getUniqueId()));
        }
    }

    public SpigotConfiguration getConfiguration() {
        return configuration;
    }
    public SpigotConfiguration getPlayerSave() {
        return playerSave;
    }

    public List<UUID> getTranslatedPlayers() {
        return translatedPlayers;
    }

    public List<Languages> getAcceptedLanguages() {
        return acceptedLanguages;
    }
}
